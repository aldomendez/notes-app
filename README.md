# Aplicacion de Notas

La intension de esta aplicacion es tener un lugar digital donde pueda poner notas de las cosas que tengo que hacer que se adapte a la forma en la que regularmente tomo notas: _de manera poco estructurada_. Pero que me permita usar la tecnologia y una pocas marcas para poder regresar a esos pensamientos fugaces mas adelante.

La aplicacion esta inspirada en una aplicacion de pago que vi la otra vez en internet y se me hizo muy interesante, pero no quiero pagar por el servicio y ademas quiero poder hacer las cosas de manera diferente a como lo tienen implementado, con mejores visuales y mucho mas ligero. Ademas no necesito la capacidad de poder agregar notas por medio de mensaje de texto como esta pensado principalmente esa aplicacion. En su lugar, espero tener una pantalla muy ligera hecha en SolidJS (actualmente unos 100 Kb, y menos de 1Kb al usar el cache en cargas subsecuentes).

## Ideas originales

Lo que me gusto de la idea original son las categorias en las que se pueden organizar las ideas:

- Usando /folders
- Pendientes
- Con #etiquetas 
- Creando !eventos
- Usando +Beans (costales de frijoles)
- agregando links
- Spells (hechizos)

Todo esto la hace muy versatil, puedes crear marcas de casi cualquier cosa con este metodo, por ejemplo yo cree una carpeta `/read` donde pongo los links o nombres de libros que quiero leer y las marco como pendiente. Con esto cuando tenga tiempo de leer ire a la aplicacion buscare lo que no he completado y lo marco como en progreso. Y al terminar de leer simplemente lo marco como completado. Y si se puede, pedo tener notas/ o comentarios de esto.

## Implementacion.

Hasta ahorita lo que tengo implementado es que puedo sacar (de manera muy burda) los datos de esas caracteristicas de una nota y lo puedo gardar en la base de datos como un objeto estructurado, junto con el texto original.

## pendientes

[x] TODO: Create the folder tree of all the notes of a user.
[x] TODO: Cache the folder tree.
[ ] TODO: Create the UI for the folder filter
[ ] TODO: use this post on model observers to create an observer where we re calculate ans store, tags, events todos, and beans. with this I can then clean the NotesController insert/update/delete methods https://www.larashout.com/how-to-use-laravel-model-observers

## tests

`npx uvu -r esm resources/scripts/components noteParser.test.js`

```js
$.ajaxSetup({
   headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }
});
```