module.exports = {
    darkMode: 'media',
    content: [
        './resources/**/*.blade.php',
        './resorces/**/*.{js,ts,tsx,jsx}',
        './resources/**/*.js',
        './resources/**/*.jsx',
        './resources/**/*.ts',
        './resources/**/*.tsx',
    ],
    theme: {
        extend: {},
    },
    plugins: [
        require('@tailwindcss/forms'),
        require('@tailwindcss/typography'),
        require('@tailwindcss/aspect-ratio'),
    ],
    // corePlugins: { preflight: false, }
}
