import { defineConfig } from "laravel-vite";
import solidPlugin from "vite-plugin-solid";
// import { fileURLToPath } from 'url'

export default defineConfig({
    plugins: [solidPlugin()],
    build: {
        target: "esnext",
        polyfillDynamicImport: false,
        // rollupOptions: {
        //   input: {
        //     appSchoool: fileURLToPath(new URL('./resources/school/index.html', import.meta.url)),
        //     appStudent: fileURLToPath(new URL('./resources/student/index.html', import.meta.url)),
        //     appAuth: fileURLToPath(new URL('./resources/auth/index.html', import.meta.url)),
        //   },
        // },
    },
});
