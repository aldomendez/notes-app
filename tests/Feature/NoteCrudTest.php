<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Note;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NoteCrudTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_retrieve_notes()
    {
        $user = User::factory()->create();
        Note::factory(5)->create(["user_id" => $user->id]);
        $response = $this->actingAs($user)->getJson('/api/notes');
        $response->assertJson([
            "data" => []
        ])->assertJsonCount(5, 'data');

        $this->assertDatabaseCount('notes', 5);

        $response->assertStatus(200);
    }

    public function test_cannot_create_an_empty_new_note()
    {
        $user = User::factory()->create();
        $note = Note::factory()->make();

        $response = $this->actingAs($user)->postJson('/api/notes');

        $response->assertJsonValidationErrors(["note_body", "folder", "todo", "full_note"]);

        $response->assertStatus(422);
    }

    public function test_can_create_new_note()
    {
        $user = User::factory()->create();
        $note = Note::factory()->make();

        $response = $this
            ->withoutExceptionHandling()
            ->actingAs($user)
            ->postJson('/api/notes', $note->toArray());

        // $response->dump();
        $response->assertJsonStructure([
            "folder",
            "todo",
            "beans" => [
                [
                    "operator",
                    "symbol",
                    "amount",
                ]
            ],
            "tags" => [],
            "events" => [],
            "bookmarks" => [],
            "note_body",
            "full_note",
            "id",
            "user_id",
        ]);
        $this->assertDatabaseCount('notes', 1);
        $this->assertDatabaseCount('beans', 2);
        $this->assertDatabaseCount('tags', 3);
        $this->assertDatabaseCount('note_tag', 3);
        $this->assertDatabaseCount('events', 3);
        $this->assertDatabaseCount('event_note', 3);

        $response->assertStatus(201);
    }

    public function test_can_update_a_note()
    {
        $user = User::factory()->create();
        $note = Note::factory()->create(["user_id" => $user->id]);

        /**
         * Updated model to test new elements
         */
        $note->folder = "/docs";
        $note->todo = "NONE";
        $note->beans = [
            [
                "operator" => "+",
                "symbol" => "cosa",
                "amount" => 1
            ],
            [
                "operator" => "+",
                "symbol" => "💰",
                "amount" => 1
            ],
            [
                "operator" => "-",
                "symbol" => "🤯",
                "amount" => 1
            ]
        ];
        $note->tags = [
            "js",
            "data",
            "browser",
            "solidjs",
            "vue",
            "docs"
        ];
        $note->events = [
            "code",
            "test",
            "docs",
        ];
        $note->bookmarks = [
            "https://tinybase.org/guides/"
        ];
        $note->note_body = "#js #data #browser #solidjs #vue #docs\\n\\nGuia para la nueva herramienta de administracion de datos\\n\\n+cosa +💰 -🤯";
        $note->full_note = "/docs #js #data #browser #solidjs #vue #docs\\n\\nGuia para la nueva herramienta de administracion de datos\\n\\n+cosa +💰 -🤯\\nhttps://tinybase.org/guides/";



        $response = $this
            ->withoutExceptionHandling()
            ->actingAs($user)
            ->putJson('/api/notes/' . $note->id, $note->toArray());

        // $response->dump();
        $response->assertJsonStructure([
            "folder",
            "todo",
            "beans" => [
                [
                    "operator",
                    "symbol",
                    "amount",
                ]
            ],
            "tags" => [],
            "events" => [],
            "bookmarks" => [],
            "note_body",
            "full_note",
            "id",
            "user_id",
        ]);
        $this->assertDatabaseCount('notes', 1);
        $this->assertDatabaseCount('beans', 3);
        $this->assertDatabaseCount('tags', 6);
        $this->assertDatabaseCount('note_tag', 6);
        $this->assertDatabaseCount('events', 3);
        $this->assertDatabaseCount('event_note', 3);

        $response->assertStatus(200);
    }

    public function test_can_delete_notes()
    {
        $user = User::factory()->create();
        Note::factory(5)->create(["user_id" => $user->id]);
        $response = $this->actingAs($user)->deleteJson('/api/notes/2');
        $response->assertStatus(204);

        $this->assertDatabaseCount('notes', 4);
        $this->assertDatabaseMissing('notes', [
            'id' => 2
        ]);
    }
}
