<?php

namespace Tests\Feature;


use Tests\TestCase;
use App\Models\Note;
use App\Models\User;
use App\Actions\Notes\CreateFolderTree;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateFolderTreeTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_get_the_folder_tree()
    {
        $folders = collect([
            '/artisan/view/command',
            '/',
            '/docs',
            '/test',
            '/docs/laravel/actions',
            '/docs/user',
            '/docs/user',
        ]);

        $tree = CreateFolderTree::create($folders);
        // dd($tree);

        $this->assertCount(3, $tree);
        $this->assertArrayHasKey('artisan', $tree);
        $this->assertArrayHasKey('view', $tree['artisan']);
        $this->assertArrayHasKey('command', $tree['artisan']['view']);
        $this->assertArrayHasKey('docs', $tree);
        $this->assertArrayHasKey('laravel', $tree['docs']);
        $this->assertArrayHasKey('actions', $tree['docs']['laravel']);
        $this->assertArrayHasKey('user', $tree['docs']);
        $this->assertArrayHasKey('test', $tree);
    }

    public function test_can_retrieve_folder_tree()
    {
        $user = User::factory()->create();

        // 10 notes from different user
        Note::factory(10)->create([
            "user_id" => 2,
            "folder" => '/this/should/not/be/part/of/the/output'
        ]);

        Note::factory()->create(["folder" => '/artisan/view/command']);
        Note::factory()->create(["folder" => '/']);
        Note::factory()->create(["folder" => '/test']);
        Note::factory()->create(["folder" => '/docs']);
        Note::factory()->create(["folder" => '/docs/laravel/actions']);
        Note::factory()->create(["folder" => '/docs/user']);
        Note::factory()->create(["folder" => '/docs/user']);

        $response = $this->actingAs($user)->getJson('api/folders');
        // $response->dump();
        $response->assertJson([
            "artisan" => [
                "view" => [
                    "command" => [],
                ],
            ],
            "docs" => [],
            "test" => [],
            "docs" => [
                'laravel' => [
                    'actions' => [],
                ],
                'user' => []
            ],
        ]);

        $response->assertJsonCount(3);
        $response->assertJsonCount(2, 'docs');

        $response->assertStatus(200);
    }
}
