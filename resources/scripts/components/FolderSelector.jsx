import { createSignal, For, Show } from "solid-js"
import { getFolders, store, setStore, getNotes } from "../src/store"
import { get } from 'lodash'

getFolders()
const search = (f) => {
    setStore('selectedFolder', f)
    getNotes()
}

function NextFolderSelector(props) {
    const [o, so] = createSignal(false)
    return (
        <div class="relative inline-block text-left">
            <div>
                <button onClick={() => so(!o())} type="button" class="bg-blue-600 rounded-sm flex items-center text-white font-bold hover:text-gray-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-blue-500" id="menu-button" aria-expanded="true" aria-haspopup="true">
                    <span class="sr-only">Open options</span>
                    {/* <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z" />
                    </svg> */}
                    <svg class="flex-shrink-0 h-5 w-5 text-gray-300" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20" aria-hidden="true">
                        <path d="M5.555 17.776l8-16 .894.448-8 16-.894-.448z" />
                    </svg>
                </button>
            </div>

            <Show when={o()}>{() => {
                let fdx = Object.keys(get(store.folders, store.selectedFolder, store.folders)).sort()
                return (
                    <div class="origin-top-right absolute left-0 mt-2 w-56 max-h-60 rounded-sm overflow-auto shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                        <div class="py-1" role="none">
                            {/* <!-- Active: "bg-gray-100 text-gray-900", Not Active: "text-gray-700" --> */}
                            <For each={fdx}>{(d) => (
                                <button onClick={() => search([...store.selectedFolder, d])} type="button" class="w-full text-left text-gray-700 inline-block px-4 py-2 text-sm hover:bg-gray-100 hover:text-gray-900 truncate" role="menuitem" tabindex="-1" id="menu-item-0">{d}</button>
                            )}</For>


                        </div>
                    </div>
                )
            }}</Show>
        </div>
    )

}

export function FolderSelector(props) {
    return (
        <nav class="flex" aria-label="Breadcrumb">
            <ol role="list" class="flex items-center space-x-1">
                <li>
                    <div>
                        <button onClick={() => search([])} type="button" class="text-gray-400 hover:text-gray-500">
                            <svg class="flex-shrink-0 h-5 w-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 7v10a2 2 0 002 2h14a2 2 0 002-2V9a2 2 0 00-2-2h-6l-2-2H5a2 2 0 00-2 2z"></path></svg>
                            <span class="sr-only">Root</span>
                        </button>
                    </div>
                </li>
                <For each={store.selectedFolder}>{(d, i) => {
                    let x = store.selectedFolder.slice(0, i() + 1)
                    return (
                        <li>
                            <div class="flex items-center">
                                <svg class="flex-shrink-0 h-5 w-5 text-gray-300" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20" aria-hidden="true">
                                    <path d="M5.555 17.776l8-16 .894.448-8 16-.894-.448z" />
                                </svg>
                                <button type="button" onClick={() => search(x)} class="ml-1 text-sm font-medium text-gray-500 hover:text-gray-700">{d}</button>
                            </div>
                        </li>
                    )
                }}</For>

                <li>
                    <NextFolderSelector></NextFolderSelector>
                </li>
            </ol>
        </nav>

    )
}
