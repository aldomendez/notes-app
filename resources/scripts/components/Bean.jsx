import { Link } from "solid-app-router";

export function Bean(props) {
    return (
        <Link replace={true} href={`/beans?bean=${props.bean.symbol}`} class="inline-flex items-center px-2 py-0.5 rounded text-xs font-medium bg-yellow-100 text-yellow-800">
            {props.bean.symbol}
            <span class="ml-1">{props.bean.operator}{props.bean.amount}</span>
        </Link>
    );
}
