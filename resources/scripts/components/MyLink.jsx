import { useHref, useLocation, useNavigate, useResolvedPath } from "solid-app-router";
import { createMemo, mergeProps, splitProps } from "solid-js";

function LinkBase(props) {
    const [, rest] = splitProps(props, ["children", "to", "href", "state", "onClick"]);
    const navigate = useNavigate();
    const href = useHref(() => props.to);
    const handleClick = evt => {
        const { onClick, to, target } = props;
        if (typeof onClick === "function") {
            onClick(evt);
        }
        else if (onClick) {
            onClick[0](onClick[1], evt);
        }
        if (to !== undefined &&
            !evt.defaultPrevented &&
            evt.button === 0 &&
            (!target || target === "_self") &&
            !(evt.metaKey || evt.altKey || evt.ctrlKey || evt.shiftKey)) {
            evt.preventDefault();
            navigate(to, {
                resolve: false,
                replace: props.replace || false,
                scroll: !props.noScroll,
                state: props.state
            });
        }
    };
    return (<a {...rest} href={href() || props.href} onClick={handleClick}>
        {props.children}
    </a>);
}
export function MyLink(props) {
    props = mergeProps({ activeClass: "active", inactiveClass: '' }, props);
    const [, rest] = splitProps(props, ["activeClass", "end"]);
    const location = useLocation();
    const to = useResolvedPath(() => props.href);
    const isActive = createMemo(() => {
        const to_ = to();
        if (to_ === undefined) {
            return false;
        }
        const path = to_.split(/[?#]/, 1)[0].toLowerCase();
        const loc = location.pathname.toLowerCase();
        return props.end ? path === loc : loc.startsWith(path);
    });
    return (<LinkBase {...rest} replace={true} to={to()} classList={{ [props.activeClass]: isActive(), [props.inactiveClass]: !isActive() }} aria-current={isActive() ? "page" : undefined} />);
}