import { Link } from "solid-app-router";

export function Event(props) {
    return (<Link replace={true} href={`/tagsevent?event=${props.children}`} class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-gray-100 text-gray-800">
        <span class="group-hover:text-gray-800 text-gray-400 mr-[2px]">!</span>{props.children}
    </Link>);
}
