// tests/demo.js
import { test } from 'uvu';
import * as assert from 'uvu/assert';

import { parseNote } from './noteParser.js'

const base = {
    folder: "/",
    todo: "NONE",
    beans: [],
    tags: [],
    events: [],
    bookmarks: [],
    note_body: "",
    full_note: ""
}

test('test can parse a folder', () => {
    assert.equal(parseNote('/dir'), {
        ...base,
        folder: '/dir',
        full_note: '/dir'
    });
    assert.equal(parseNote('/dir-with-dashes'), {
        ...base,
        folder: "/dir-with-dashes",
        full_note: "/dir-with-dashes"
    });
});

test('can parse the todo part', () => {
    assert.equal(parseNote('/dir tODo'), {
        ...base,
        folder: "/dir",
        todo: "TODO",
        full_note: "/dir tODo"
    });
    assert.equal(parseNote('/dir doNe'), {
        ...base,
        folder: "/dir",
        todo: "DONE",
        full_note: "/dir doNe"
    });
    assert.equal(parseNote('todo'), {
        ...base,
        todo: "TODO",
        full_note: "todo"
    });
});
test('can find beans', () => {
    assert.equal(parseNote(' +amore-amore_1234567890 -amore'), {
        ...base,
        beans: [
            {
                operator: "+",
                symbol: "amore-amore_1234567890",
                amount: 1
            }, {
                operator: "-",
                symbol: "amore",
                amount: 1
            }
        ],
        note_body: "+amore-amore_1234567890 -amore",
        full_note: " +amore-amore_1234567890 -amore",
    });
});
test('can find beans', () => {
    let note = ' +🍾 -bag \n\n\n +🍾:50.25 -bean:12.8 -💰 -🏷'
    assert.equal(parseNote(note), {
        ...base,
        beans: [
            {
                operator: "+",
                symbol: "🍾",
                amount: 1
            }, {
                operator: "-",
                symbol: "bag",
                amount: 1
            }, {
                operator: "+",
                symbol: "🍾",
                amount: 50.25
            }, {
                operator: "-",
                symbol: "bean",
                amount: 12.8
            }, {
                operator: "-",
                symbol: "💰",
                amount: 1
            }, {
                operator: "-",
                symbol: "🏷",
                amount: 1
            }
        ],
        note_body: note.trim(),
        full_note: note,
    });
});
test('can find events', () => {
    let event = ' !amore !example \n  !event-with_dashes-1234567890'
    assert.equal(parseNote(event), {
        ...base,
        events: [
            'amore',
            'example',
            'event-with_dashes-1234567890',
        ],
        note_body: event.trim(),
        full_note: event,
    });
});
test('can find tags', () => {
    let note = ' #amore \n#example #example-with_dashes'
    assert.equal(parseNote(note), {
        ...base,
        tags: [
            'amore',
            'example',
            'example-with_dashes',
        ],
        note_body: note.trim(),
        full_note: note,
    });
});

test.run();