import { createEffect, createSignal } from 'solid-js'
import { Link } from 'solid-app-router'
import { parseNote } from './noteParser.js'

export function NoteEditor(props) {
    // createEffect(() => {
    //     setNote(parseNote(text()))
    // })
    const [text, setText] = createSignal(props.text ?? '')


    let search = new URLSearchParams(window.location.search)
    if (search.has('url')) {
        setText(`/bookmark todo\n\n${search.get('title')}\n\n${search.get('url')}`)
    }

    if (window.location)
        return (
            <div class="bg-gray-50 px-4 py-6 sm:px-6 shadow rounded-sm">
                <div class="flex space-x-3">
                    <div class="min-w-0 flex-1">
                        {/* <pre class='text-xs'>{JSON.stringify(text(), null, 2)}</pre>
                    <pre class='text-xs'>{JSON.stringify(note, null, 2)}</pre> */}
                        <form onSubmit={e => {
                            e.preventDefault()
                            props.save?.(parseNote(text()))
                            setText('')
                        }}>
                            <div>
                                <label for="note" class="sr-only">About</label>
                                <textarea value={text() || ''} onInput={e => setText(e.target.value)} id="note" name="note" rows="10" class="shadow-md block w-full focus:ring-blue-500 focus:border-blue-500 sm:text-sm border border-gray-300 rounded-sm p-2" placeholder="Add a note"></textarea>
                            </div>
                            <div class="mt-3 flex items-center justify-between">
                                <Link href="/help" class="group inline-flex items-start text-sm space-x-2 text-gray-500 hover:text-gray-900">
                                    {/* <!-- Heroicon name: solid/question-mark-circle --> */}
                                    <svg class="flex-shrink-0 h-5 w-5 text-gray-400 group-hover:text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd" />
                                    </svg>
                                    <span>
                                        You can add links #tags !events +add or -withdraw beans in this box.
                                    </span>
                                </Link>
                                <button type="submit" class="inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm font-medium rounded-sm text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
                                    Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
}
