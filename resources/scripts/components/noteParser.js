// Emoji matcher
// (\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])
// const label_pattern = '(?:(?:"(?:[^"\\\\]*(?:\\\\.[^"\\\\]*)*?)")|(?:(?:[\u1000-\uffff]|[a-zA-Z]|[\$\xA2-\xA5\u058F\u060B\u09F2\u09F3\u09FB\u0AF1\u0BF9\u0E3F])(?:[\u1000-\uffff]|[a-zA-Z0-9\-_]|[\$\xA2-\xA5\u058F\u060B\u09F2\u09F3\u09FB\u0AF1\u0BF9\u0E3F]){0,63}))'

// const number_pattern = '(?:(?:[0-9]*\\.?[0-9]+|[0-9]+\\.?[0-9]*)(?:[eE][+-]?[0-9]+)?)'

const folderReg = /^\/[\w\/-]+/g

let beanReg = /[ \n\^]([\+-])((?:(?:\"(?:[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*?)\")|(?:(?:[\u1000-\uffff]|[a-zA-Z]|[$\xA2-\xA5\u058F\u060B\u09F2\u09F3\u09FB\u0AF1\u0BF9\u0E3F])(?:[\u1000-\uffff]|[a-zA-Z0-9-_]|[$\xA2-\xA5\u058F\u060B\u09F2\u09F3\u09FB\u0AF1\u0BF9\u0E3F]){0,63})))\:?([\d]*\.?\d*)/gim

const tagsReg = /[ \n][#]{1}((?:\w|-|:|\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])+)/gm

const eventsReg = /[ \n][!]{1}((?:\w|-|:|\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])+)/gm

const todoReg = /^[\/\w]* *(TODO|DONE)/gi

const cleanerReg = /^[\/\w\-]* *(TODO|DONE)*/gi
const cleanerBookmarkReg = /(https?:\/\/[\w.\/\-?&=%#+]+)/gi

const bookmarkReg = /[ \n]?(https?:\/\/[\w.\/\-?&=%#+]+)/gi


export function parseNote(full_note = '') {
    // Folder content
    let folder = full_note.match(folderReg)
    let todo = Array.from(full_note.matchAll(todoReg))
    let note_body = full_note.replace(cleanerReg, '').replace(cleanerBookmarkReg, '').trim()
    return {
        folder: folder ? folder[0] : '/',
        todo: todo.length > 0 ? todo[0][1].toUpperCase() : 'NONE',
        beans: Array.from(full_note.matchAll(beanReg), m => ({ operator: m[1], symbol: m[2], amount: m[3] ? +m[3] : 1, })),
        tags: Array.from(full_note.matchAll(tagsReg), m => m[1]),
        events: Array.from(full_note.matchAll(eventsReg), m => m[1]),
        bookmarks: Array.from(full_note.matchAll(bookmarkReg), m => m[1]),
        note_body,
        full_note,
    }

}