import { For } from "solid-js";
import { formatDistance } from 'date-fns';
import { Bean } from "./Bean";
import { Tag } from "./Tag";
import { Link } from "solid-app-router";

export function NotesGrid(props) {
    return (
        <div class="mt-6 pt-4 grid gap-1 md:grid-cols-2 lg:grid-cols-3 md:gap-x-2 lg:gap-y-2">
            <For each={props.notes}>{(note) => {
                const folderSearch = new URLSearchParams()
                folderSearch.set('folder', note.folder.slice(1))
                return (
                    <div class="group max-w-full p-2 bg-slate-50 flex flex-col rounded-sm border border-slate-100 hover:border-slate-400">
                        <button onClick={() => props.onClick?.(note)} type="button" class="block max-w-full text-left">
                            <p class="mt-3 text-base text-gray-500 font-light max-w-full">
                                {note.note_body}
                            </p>
                        </button>
                        <div class="text-sm text-gray-500 w-full text-left">
                            <For each={note.beans}>{(bean) => <Bean bean={bean} />}</For>
                        </div>
                        <div class="text-sm text-gray-500 w-full text-left">
                            <For each={note.tags}>{(t) => (<Tag>{t}</Tag>)}</For>
                        </div>
                        <div class="mt-3  w-full text-left">

                            <For each={note.bookmarks}>{(bm) => {
                                let t = new URL(bm);
                                return (<a target="_blank" href={bm} label={bm} class="text-ellipsis max-w-full w-full block text-xs font-light text-blue-500 hover:text-blue-700 group">
                                    {t.host}<span class="text-gray-400 group-hover:text-blue-700">{t.pathname}</span>
                                </a>);
                            }}</For>
                            <div class="text-xs text-gray-400 w-full text-left">
                                <time datetime="2020-03-16">{formatDistance(new Date(note.created_at), new Date(), { addSuffix: true })}</time>
                                &nbsp;<Link class="text-ellipsis" href={`?${folderSearch}`}>{note.folder}</Link>
                            </div>
                        </div>
                    </div>
                )
            }}</For>


        </div>
    );
}
