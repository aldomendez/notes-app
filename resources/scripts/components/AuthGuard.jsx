import { Show } from "solid-js";
import Login from "../pages/Login";
import { auth } from "../src/store";

export function AuthGuard(props) {
    return (
        <Show when={auth.user != null} fallback={() => <Login></Login>}>
            {props.children}
        </Show>
    )
}