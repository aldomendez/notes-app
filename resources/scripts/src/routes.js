import { lazy } from "solid-js";

export const routes = [
    {
        path: '/',
        component: lazy(() => import('../pages/Home')),
    }, {
        path: '/new',
        component: lazy(() => import('../pages/NewNote')),
    }, {
        path: '/help',
        component: lazy(() => import('../pages/Help')),
    }, {
        path: '/beans',
        component: lazy(() => import('../pages/Beans')),
    }, {
        path: '/tags',
        component: lazy(() => import('../pages/Tags')),
    }, {
        path: '/events',
        component: lazy(() => import('../pages/Events')),
    }, {
        path: '/todos',
        component: lazy(() => import('../pages/Todos')),
    }, {
        path: '/password/reset',
        component: lazy(() => import('../pages/PasswordReset')),
    }, {
        path: '/login',
        component: lazy(() => import('../pages/Login')),
    }, {
        path: '/*',
        component: lazy(() => import('../pages/404')),
    }
];
