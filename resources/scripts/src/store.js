import { createEffect, createSignal } from 'solid-js'
import { createStore } from 'solid-js/store'
// import { useNavigate } from 'solid-app-router';
// const navigate = useNavigate();
const emptyNote = {
    id: "",
    note_body: "",
    folder: "/",
    date: "",
    tags: [],
    events: [],
    beans: [],
    bookmarks: [],
    todo: "NONE",
    full_note: "",
}
let userExample = {
    id: 1,
    name: "Rubie Kuvalis",
    email: "aldomendez86@gmail.com",
    email_verified_at: "2022-01-03T03:40:30.000000Z",
    created_at: "2022-01-03T03:40:30.000000Z",
    updated_at: "2022-01-03T03:40:30.000000Z",
    two_factor_secret: null,
    two_factor_recovery_codes: null
}
export const [note, setNote] = createStore({ ...emptyNote })
export const [auth, setAuth] = createStore({
    user: null,
})
export const [search, setSearch] = createSignal()
export const [store, setStore] = createStore({
    notes: [],
    tags: ['❤️', '🤘', '👍', '‼', '❓', '😂', 'important', 'sandra', 'helena', 'oscar', 'papa'],
    search: '',
    folders: {},
    selectedFolder: []
})

/**
 * Endpoint APIS
 */

const loginApi = '/login'
const userApi = '/api/user'
const folderApi = '/api/folders'
const resource = '/api/notes'

const config = {
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    // mode: 'cors',
    // credentials: 'same-origin',
    method: 'get',
}

const csrfted = (fn) => fn()
// fetch('sanctum/csrf-cookie').then(async res => {
//     console.log(...res.headers);
//     fn()
// })

const throwErrorCode = async (res) => {
    // console.log(...res.headers);
    if (res.ok) {
        return res
    } else {
        if (res.status === 401) {
            setAuth('user', null)
        }
        // console.log(res.status);
        // console.log(res.statusText);
        // console.log(res.type);
        // console.log(await res.json());
        throw res
    }
}

const fetchUser = () => fetch(userApi, config)
    .then(throwErrorCode)
    .catch(err => {
        console.log('error');
        console.log(err);
        throw err
    })
    .then(async d => {
        let r = await d.json()
        console.log(r);
        setAuth('user', r)
        getNotes()
        return r
    })

fetchUser()

export function login({ email, password, remember }) {
    return fetch(loginApi,
        {
            ...config,
            method: 'post',
            body: JSON.stringify({ email, password, remember }),
        })
        .then(throwErrorCode)
        .catch(err => {
            console.log('error');
            console.log(err);
            throw err
        })
        .then(async d => {
            fetchUser()
            let r = await d.json()
            console.log(r);
            setStore('notes', r.data)
            return r
        })
}

export function getNotes() {
    const s = new URLSearchParams()
    s.append('folder', store.selectedFolder.join('/'))
    s.append('search', store.search)
    return fetch(resource + '?' + s.toString(), config)
        .then(throwErrorCode)
        .catch(err => {
            console.log('error');
            console.log(err);
            return err
        })
        .then(async d => {
            let r = await d.json()
            console.log(r);
            setStore('notes', r.data)
            // getFolders()
            return r
        })
}
export function getFolders() {
    return fetch(folderApi, config)
        .then(throwErrorCode)
        .catch(err => {
            console.log('error');
            console.log(err);
            return err
        })
        .then(async d => {
            let r = await d.json()
            console.log(r);
            setStore('folders', r)
            return r
        })
}

export function saveNote(note) {
    return fetch('/api/notes',
        {
            ...config,
            method: 'post',
            body: JSON.stringify(note)
        })
        .then(throwErrorCode)
        .then(async d => {
            let r = await d.json()
            setStore('notes', n => ([{ ...r }, ...n]))
            getFolders()
            return r
        })
}

export function updateNote(id, note) {
    return fetch('/api/notes/' + id,
        {
            ...config,
            method: 'put',
            body: JSON.stringify(note)
        })
        .then(throwErrorCode)
        .then(async d => {
            getFolders()
            let r = await d.json()
            return r
        })
}


export function deleteNote(id) {
    return fetch('/api/notes/' + id,
        {
            ...config,
            method: 'delete',
        })
        .then(throwErrorCode)
        .then(async d => {
            setStore('notes', n => n.filter(d => d.id != id))
            return d
        })
}

