import { Router } from "solid-app-router";
import { render } from "solid-js/web";

import "../css/app.css";
import App from "./src/app";

render(() => (
    <Router>
        <App />
    </Router>
), document.getElementById("app"));
