import { Link } from "solid-app-router";
import { NoteEditor } from "../components/NoteEditor";
import { note, saveNote } from "../src/store";


export default function NewNote(props) {
    function addNote(note) {
        saveNote(note)
    }
    return (
        <div>
            <Link href="/" class="bg-blue-200 p-2">Back</Link>
            <NoteEditor text={note.full_note} save={note => addNote(note)}></NoteEditor>
        </div>
    )
}
