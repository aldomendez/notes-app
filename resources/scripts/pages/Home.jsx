import { Link, useSearchParams } from "solid-app-router";
import { createEffect, createSignal, For, Show } from "solid-js";
import { store, getNotes, updateNote, setStore, deleteNote, getFolders } from '../src/store'
import { formatDistance } from 'date-fns'
import { NoteEditor } from "../components/NoteEditor";
import NavBar from "../components/Navbar";
import { Bean } from "../components/Bean";
import { Tag } from "../components/Tag";
import { Event } from '../components/Event'
import { NotesGrid } from "../components/NotesGrid";
import { AuthGuard } from "../components/AuthGuard";
import { FolderSelector } from "../components/FolderSelector";



const [note, setNote] = createSignal(null)
const [text, setText] = createSignal(null)
let editingNote = null

// createEffect(() => {
// if (store.notes.length > 0) {
//     setNote(store.notes[0])
// }
// })

function QuickEdit(props) {

    return (
        <Show when={text() != null}>
            <div class="fixed z-10 inset-0 overflow-y-auto" role="dialog" aria-modal="true">
                <div class="flex min-h-screen text-center md:block md:px-2 lg:px-4" style="font-size: 0;">
                    {/* <!--
                Background overlay, show/hide based on modal state.

                Entering: "ease-out duration-300"
                From: "opacity-0"
                To: "opacity-100"
                Leaving: "ease-in duration-200"
                From: "opacity-100"
                To: "opacity-0"
    --> */}
                    <div class="hidden fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity md:block" aria-hidden="true"></div>

                    {/* <!-- This element is to trick the browser into centering the modal contents. --> */}
                    <span class="hidden md:inline-block md:align-middle md:h-screen" aria-hidden="true">&#8203;</span>

                    {/* <!--
                Modal panel, show/hide based on modal state.

                Entering: "ease-out duration-300"
                From: "opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
                To: "opacity-100 translate-y-0 md:scale-100"
                Leaving: "ease-in duration-200"
                From: "opacity-100 translate-y-0 md:scale-100"
                To: "opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
    --> */}
                    <div class="flex text-base text-left transform transition w-full md:inline-block md:max-w-2xl md:px-4 md:my-8 md:align-middle lg:max-w-4xl">
                        <div class="w-full relative flex items-center bg-white px-4 pt-14 pb-8 overflow-hidden shadow-2xl sm:px-6 sm:pt-8 md:p-6 lg:p-8">
                            <button onClick={() => setText(null)} type="button" class="absolute top-4 right-4 text-gray-400 hover:text-gray-500 sm:top-8 sm:right-6 md:top-6 md:right-6 lg:top-8 lg:right-8">
                                <span class="sr-only">Close</span>
                                {/* <!-- Heroicon name: outline/x --> */}
                                <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            </button>

                            <div class="w-full grid grid-cols-1 gap-y-8 gap-x-6 items-start lg:gap-x-8">

                                <NoteEditor text={text()} save={note => {
                                    updateNote(editingNote.id, note).then(nt => {
                                        let i = store.notes.findIndex(n => editingNote.id == n.id)
                                        setStore('notes', i, nt)
                                    })
                                    setText(null)
                                }}></NoteEditor>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Show>

    )
}
function QuickView(props) {
    return (
        <Show when={note() != null}>
            <div class="fixed z-10 inset-0 overflow-y-auto" role="dialog" aria-modal="true">
                <div class="flex min-h-screen text-center md:block md:px-2 lg:px-4" style="font-size: 0;">
                    {/* <!--
                Background overlay, show/hide based on modal state.

                Entering: "ease-out duration-300"
                From: "opacity-0"
                To: "opacity-100"
                Leaving: "ease-in duration-200"
                From: "opacity-100"
                To: "opacity-0"
    --> */}
                    <div class="hidden fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity md:block" aria-hidden="true"></div>

                    {/* <!-- This element is to trick the browser into centering the modal contents. --> */}
                    <span class="hidden md:inline-block md:align-middle md:h-screen" aria-hidden="true">&#8203;</span>

                    {/* <!--
                Modal panel, show/hide based on modal state.

                Entering: "ease-out duration-300"
                From: "opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
                To: "opacity-100 translate-y-0 md:scale-100"
                Leaving: "ease-in duration-200"
                From: "opacity-100 translate-y-0 md:scale-100"
                To: "opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
    --> */}
                    <div class="flex text-base text-left transform transition w-full md:inline-block md:max-w-2xl md:px-4 md:my-8 md:align-middle lg:max-w-4xl">
                        <div class="w-full relative flex items-center bg-white px-4 pt-14 pb-8 overflow-hidden shadow-2xl sm:px-6 sm:pt-8 md:p-6 lg:p-8">
                            <button onClick={() => setNote(null)} type="button" class="absolute top-4 right-4 text-gray-400 hover:text-gray-500 sm:top-8 sm:right-6 md:top-6 md:right-6 lg:top-8 lg:right-8">
                                <span class="sr-only">Close</span>
                                {/* <!-- Heroicon name: outline/x --> */}
                                <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            </button>

                            <div class="w-full grid grid-cols-1 gap-y-8 gap-x-6 items-start lg:gap-x-8">
                                <div class="sm:col-span-8 lg:col-span-7">

                                    <section aria-labelledby="information-heading" class="mt-3">
                                        <h3 id="information-heading" class="sr-only">Product information</h3>

                                        <div class="flex justify-between">
                                            <p class="text-lg text-gray-900">{note().folder}</p>
                                            <p class="text-lg text-gray-900">{formatDistance(new Date(note().created_at), new Date(), { addSuffix: true })}</p>
                                        </div>


                                        <div class="mt-6 bg-gray-100">
                                            <h4 class="sr-only">Description</h4>

                                            <div class="m-4 prose prose-indigo prose-sm w-full text-gray-900">
                                                <For each={note().note_body.split('\n')}>{(p) => (
                                                    <p>
                                                        {p}
                                                    </p>
                                                )}</For>

                                            </div>
                                        </div>
                                    </section>

                                    <section aria-labelledby="options-heading" class="mt-6">
                                        <h3 id="options-heading" class="sr-only">Product options</h3>

                                        <form>
                                            <Show when={note().tags.length > 0}>{() => (
                                                <div>
                                                    <h4 class="text-sm text-gray-600">Tags</h4>

                                                    <div>
                                                        <For each={note().tags}>{(t) => (<Tag>{t}</Tag>)}</For>
                                                    </div>
                                                </div>
                                            )}</Show>
                                            <Show when={note().events.length > 0}>{() => (
                                                <div>
                                                    <h4 class="text-sm text-gray-600">Events</h4>

                                                    <div>
                                                        <For each={note().events}>{(t) => (<Event>{t}</Event>)}</For>
                                                    </div>
                                                </div>
                                            )}</Show>
                                            <Show when={note().beans.length > 0}>{() => (
                                                <div>
                                                    <h4 class="text-sm text-gray-600">Beans</h4>

                                                    <div>
                                                        <For each={note().beans}>{(bean) => (<Bean bean={bean} />)}</For>
                                                    </div>
                                                </div>
                                            )}</Show>
                                            <Show when={note().bookmarks.length > 0}>{() => (
                                                <div>
                                                    <h4 class="text-sm text-gray-600">Bookmarks</h4>

                                                    <div class="flex flex-col">
                                                        <For each={note().bookmarks}>{
                                                            (bm) => {
                                                                let t = new URL(bm)
                                                                return (
                                                                    <a target="_blank" href={bm} label={bm} class="text-xs font-light text-blue-500 hover:text-blue-700 group">
                                                                        {t.host}<span class="text-gray-400 group-hover:text-blue-700">{t.pathname}</span>
                                                                    </a>
                                                                )
                                                            }
                                                        }</For>
                                                    </div>
                                                </div>
                                            )}</Show>

                                            <div class="bg-gray-50 px-4 py-3 justify-between sm:px-6 sm:flex sm:flex-row-reverse">
                                                <button type="button" onClick={() => {
                                                    editingNote = note()
                                                    console.log(editingNote);
                                                    setText(note().full_note)
                                                    setNote(null)
                                                }} class="w-full inline-flex justify-center rounded-sm border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:w-auto sm:text-sm">
                                                    Update
                                                </button>
                                                <button type="button" onClick={() => {
                                                    deleteNote(note().id)
                                                    setNote(null)
                                                }} class="mt-3 w-full inline-flex justify-center rounded-sm border border-transparent shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 sm:mt-0 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm">
                                                    Delete
                                                </button>
                                            </div>
                                        </form>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Show>

    )
}

const Home = () => {
    const [searchParams, setSearchParams] = useSearchParams();
    const [open, setOpen] = createSignal(false)

    return (
        <AuthGuard>
            <NavBar></NavBar>
            <div class="bg-white pt-16 pb-20 px-4 sm:px-6 lg:pt-24 lg:pb-28 lg:px-8">
                <div class="relative w-full max-w-3xl mx-auto  lg:max-w-7xl">

                    <div class="pb-5 sm:flex sm:items-center sm:justify-between">
                        <h3 class="text-3xl tracking-tight font-extrabold text-gray-900 sm:text-4xl">
                            Recent notes
                        </h3>
                        <div class="mt-3 flex sm:mt-0 sm:ml-4">
                            <a href={`javascript:u=location.href;t=document.title;void(open('${window.location.origin}/new?url=' + encodeURIComponent(u) + '&title=' + encodeURIComponent(t),'/tap','toolbar=no,width=500,height=500'))`} class="inline-flex items-center px-4 py-2 border border-gray-300 rounded-sm shadow-sm text-sm font-medium text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
                                +Note
                            </a>
                            <button onClick={() => getNotes()} type="button" class="inline-flex items-center px-4 py-2 border border-gray-300 rounded-sm shadow-sm text-sm font-medium text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
                                Refresh
                            </button>
                            <Link href="/new" class="ml-3 inline-flex items-center px-4 py-2 border border-transparent rounded-sm shadow-sm text-sm font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
                                Create
                            </Link>
                        </div>
                    </div>

                    <FolderSelector></FolderSelector>
                    <NotesGrid notes={store.notes} onClick={(note) => setNote(note)}></NotesGrid>

                </div>
            </div>
            <QuickView></QuickView>
            <QuickEdit></QuickEdit>
        </AuthGuard>

    )
};

export default Home;
