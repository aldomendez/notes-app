<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <title>
        Notes
    </title>
    <style>
        html {
            font-family: "DOS", "monospace" !important;
            font-size: 18px;
            box-sizing: border-box;
        }
    </style>
    @vite('tui')
</head>

<body>
    <div id="app"></div>
    <!-- @if(!empty($siteConfig))
    <script>
        window.siteConfig = {{ Illuminate\Support\Js::from($siteConfig) }};
    </script>
    @endif -->
</body>

</html>