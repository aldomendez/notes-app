<?php

namespace App\Providers;


use Monolog\Logger;
use Illuminate\Support\Facades\DB;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // if (config('app.env') != 'production') {
        //     DB::listen(function ($query) {
        //         $queryLog = new Logger('query');
        //         $queryLog->pushHandler(new StreamHandler(storage_path('logs/query.log')), Logger::INFO);
        //         $queryLog->info(preg_replace_array('/\?/', $query->bindings, $query->sql) . " | time:" . $query->time . " | conn: " . $query->connection->getName());
        //     });
        // }
    }
}
