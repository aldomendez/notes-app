<?php

namespace App\Models;

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Note extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $casts = [
        "note_body" => 'string',
        "folder" => 'string',
        "date" => 'datetime',
        "tags" => 'array',
        "events" => 'array',
        "beans" => 'array',
        "bookmarks" => 'array',
        "todo" => 'string',
        "full_note" => 'string',
    ];

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            Cache::forget('folders/' . $model->user_id);
        });
        static::deleted(function ($model) {
            Cache::forget('folders/' . $model->user_id);
        });
        static::created(function ($model) {
            Cache::forget('folders/' . $model->user_id);
        });
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function events()
    {
        return $this->belongsToMany(Event::class);
    }
}
