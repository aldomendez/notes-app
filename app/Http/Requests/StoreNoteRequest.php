<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'note_body' => ['required', 'string'],
            'folder' => ['required', 'string'],
            'tags' => ['array'],
            'events' => ['array'],
            'beans' => ['array'],
            'todo' => ['required', 'string'],
            'bookmarks' => ['array'],
            'full_note' => ['required', 'string'],
        ];
    }
}
