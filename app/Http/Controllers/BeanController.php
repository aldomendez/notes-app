<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBeanRequest;
use App\Http\Requests\UpdateBeanRequest;
use App\Models\Bean;

class BeanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBeanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBeanRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bean  $bean
     * @return \Illuminate\Http\Response
     */
    public function show(Bean $bean)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bean  $bean
     * @return \Illuminate\Http\Response
     */
    public function edit(Bean $bean)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBeanRequest  $request
     * @param  \App\Models\Bean  $bean
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBeanRequest $request, Bean $bean)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bean  $bean
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bean $bean)
    {
        //
    }
}
