<?php

namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Actions\Notes\CreateFolderTree;

class FolderTreeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $user = $request->user();

        return Cache::rememberForever('folders/' . $user->id, function () use ($user) {
            $folders = Note::where('user_id', $user->id)->get('folder')->pluck('folder');
            return CreateFolderTree::create($folders);
        });;
    }
}
