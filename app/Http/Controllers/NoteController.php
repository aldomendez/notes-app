<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Bean;
use App\Models\Note;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Requests\StoreNoteRequest;
use App\Http\Requests\UpdateNoteRequest;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        return Note::query()
            ->when($request->input('search'), function ($q, $search) {
                return $q->where('full_note', 'like', '%' . $search . '%');
            })
            ->when($request->input('folder'), function ($q, $folder) {
                return $q->where('folder', 'like', '/' . $folder . '%');
            })
            ->where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->simplePaginate(30)
            ->withQueryString();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreNoteRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNoteRequest $request)
    {


        $user = $request->user();
        $note = Note::create(array_merge(
            $request->validated(),
            ['user_id' => $user->id],
        ));

        /**
         * Crea y soncroniza los Eventos
         */
        $events = collect($request->validated()['events'])->map(function ($t) use ($user) {
            return ['name' => $t, 'user_id' => $user->id];
        })->toArray();

        Event::upsert($events, ['name', 'user_id']);

        $eEvents = Event::whereIn('name', $request->validated()['events'])->get()->pluck('id')->toArray();
        // dd($eTags);
        $note->events()->sync($eEvents);

        /**
         * Crea y soncroniza los Tags
         */
        $tags = collect($request->validated()['tags'])->map(function ($t) use ($user) {
            return ['name' => $t, 'user_id' => $user->id];
        })->toArray();

        Tag::upsert($tags, ['name', 'user_id']);

        $eTags = Tag::whereIn('name', $request->validated()['tags'])->get()->pluck('id')->toArray();
        // dd($eTags);
        $note->tags()->sync($eTags);

        // dd($request->validated()['tags']);
        // dd($note);
        collect($request->validated()['beans'])->map(
            function ($b) use ($note, $user) {
                return Bean::create(array_merge(
                    $b,
                    [
                        "user_id" => $user->id,
                        "note_id" => $note->id,
                    ]
                ));
            }
        );


        return $note;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function show(Note $note)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateNoteRequest  $request
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNoteRequest $request, Note $note)
    {

        $user = $request->user();
        $note->update($request->validated());

        /**
         * Crea y soncroniza los Eventos
         */
        $events = collect($request->validated()['events'])->map(function ($t) use ($user) {
            return ['name' => $t, 'user_id' => $user->id];
        })->toArray();

        Event::upsert(
            $events,
            ['name', 'user_id']
        );

        $eEvents = Event::whereIn('name', $request->validated()['events'])->get()->pluck('id')->toArray();
        // dd($eTags);
        $note->events()->sync($eEvents);

        /**
         * Crea y soncroniza los Tags
         */
        $tags = collect($request->validated()['tags'])->map(function ($t) use ($user) {
            return ['name' => $t, 'user_id' => $user->id];
        })->toArray();

        Tag::upsert($tags, ['name', 'user_id']);

        $eTags = Tag::whereIn('name', $request->validated()['tags'])->get()->pluck('id')->toArray();
        // dd($eTags);
        $note->tags()->sync($eTags);

        collect($request->validated()['beans'])->map(function ($b) use ($note, $user) {
            return Bean::create(array_merge(
                $b,
                [
                    "user_id" => $user->id,
                    "note_id" => $note->id,
                ]
            ));
        });

        // dd($request->validated()['tags']);
        // dd($note);
        Cache::forget('folders/' . $user->id);

        return $note;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function destroy(Note $note)
    {
        Cache::forget('folders/' . $note->user_id);
        // TODO borrar beans, tag and events
        $note->delete();
        return response(null, 204);
    }
}
