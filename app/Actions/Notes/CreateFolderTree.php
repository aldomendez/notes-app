<?php

namespace App\Actions\Notes;

use Illuminate\Support\Collection;

class CreateFolderTree
{
    static function create(Collection $folders)
    {
        $tree = $folders
            ->map(function ($d) {
                return substr($d, 1);
            })->filter(function ($d) {
                return $d;
            })
            ->map(function ($d) {
                return explode('/', $d);
            })
            ->reduce(function ($c, $d) {
                return static::buildTree(
                    $c,
                    collect($d)->filter(function ($d) {
                        return $d;
                    })->values()->toArray(),
                    0
                );
            }, []);

        return $tree;
    }

    static function buildTree(array $tree, $els, $deep)
    {
        $k = $els[$deep];

        if (!array_key_exists($k, $tree)) {
            $tree[$k] = [];
        }

        if (($deep + 1) <= (count($els) - 1)) {
            $tree[$k] = static::buildTree($tree[$k], $els, $deep + 1);
        }

        return $tree;
    }
}
