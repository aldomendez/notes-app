<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class NoteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "folder" => "/read",
            "todo" => "TODO",
            "beans" => [
                [
                    "operator" => "+",
                    "symbol" => "algo",
                    "amount" => 5000
                ],
                [
                    "operator" => "-",
                    "symbol" => "otra",
                    "amount" => 300
                ]
            ],
            "tags" => [
                "tag1",
                "tag2",
                "tag3",
            ],
            "events" => [
                "event1",
                "event2",
                "event3",
            ],
            "bookmarks" => [
                "https://engineering.linecorp.com/en/blog/the-baseline-for-web-development-in-2022/"
            ],
            "note_body" => "The baseline for web development in 2022 \n!event \n#tag \n+algo:5000 -otra:300",
            "full_note" => "/read todo\n\nThe baseline for web development in 2022 \n!event \n#tag \n+algo:5000 -otra:300\n\nhttps://engineering.linecorp.com/en/blog/the-baseline-for-web-development-in-2022/",
            "user_id" => 1,
        ];
    }
}
